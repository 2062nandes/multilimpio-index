'use strict';

var vm = new Vue({
  el: '#multilimpio',
  data: {
    menutemporal: false,
    formSubmitted: false,
    jobSubmitted: false,
    vue: {
      nombre: '',
      telefono: '',
      movil: '',
      ciudad: '',
      direccion: '',
      email: '',
      mensaje: '',
      envio: ''
    },
    job: {
      nombre: '',
      telefono: '',
      movil: '',
      direccion: '',
      email: '',
      experiencia: '',
      envio: ''
    }
  },
  mounted: function mounted() {
    var myLazyLoad = new LazyLoad();

    var sections = document.querySelectorAll('article [id]');
    var navLinks = document.querySelectorAll('.aside__main .aside__item a');
  },
  created: function created() {
    var wow = new WOW({
      boxClass: 'wow',
      animateClass: 'animated',
      offset: 0,
      mobile: false,
      live: false
    });
    wow.init();
    var resize = function resize() {
      var sticky = new Sticky('.aside__content');
    };
    addEventListener('resize', resize);
    addEventListener('DOMContentLoaded', resize);
    //FOOTER AÑO
    function getDate() {
      var today = new Date();
      var year = today.getFullYear();
      document.getElementById("currentDate").innerHTML = year;
    }
    getDate();
  },
  methods: {
    showModal: function showModal() {
      if (this.showmodal == true) {
        this.showmodal = false;
        document.body.style.overflowY = 'initial';
      } else {
        this.showmodal = true;
        document.body.style.overflowY = 'hidden';
      }
    },
    sideMain: function sideMain() {
      if (this.menutemporal == true) {
        this.menutemporal = false;
        console.log("DESACTIVADO");
      } else {
        this.menutemporal = true;
        console.log("ACTIVO");
      }
    },
    isFormValid: function isFormValid() {
      return this.nombre != '';
    },
    submitForm: function submitForm() {
      if (!this.isFormValid()) return;
      this.formSubmitted = true;
      this.$http.post('/mail.php', { vue: this.vue }).then(function (response) {
        this.vue.envio = response.data;
        this.clearForm();
      }, function () {});
    },
    submitJob: function submitJob() {
      if (!this.isFormValid()) return;
      this.jobSubmitted = true;
      this.$http.post('/solicitud-trabajo.php', { job: this.job }).then(function (response) {
        this.job.envio = response.data;
        this.clearForm();
      }, function () {});
    },
    clearForm: function clearForm() {
      this.vue.nombre = '';
      this.vue.email = '';
      this.vue.telefono = '';
      this.vue.movil = '';
      this.vue.direccion = '';
      this.vue.ciudad = '';
      this.vue.mensaje = '';
      this.vue.formSubmitted = false;
      this.job.nombre = '';
      this.job.email = '';
      this.job.telefono = '';
      this.job.movil = '';
      this.job.direccion = '';
      this.job.experiencia = '';
      this.job.salario = '';
      this.job.jobSubmitted = false;
    }
  }
});